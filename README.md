# Note: The application must be run in unison with the client application!

## Summary

The requested server application was implemented using the Nodejs Express framework.
In the overall application extra care was taken to use Typescript everywhere.
It exposes a get route for our client to do the api call to get the data it needs.
The server does its api calls to the Tibber Graphql api server. It automatically logs in the user and
does the authorization behind the scenes. After the user gets authorized, a request to fetch the weather data is
made. The server has also a simple cache in place to minimize requests overhead. It is a memory cache that uses time
invalidation. The cache gets invalidated after one hour.

Please follow the instructions below to run the application.

## Run the application

Start the server application:

In the server repository (https://gitlab.com/tibber1/tibber-server) run `npm run start:dev`.

After the developer server has started running properly. You can visit in a browser `localhost:6060`
to see the data fetched. If you want to see the user interface implemented then you need also to run the
client application.

Start the client application:

Go to the client repository (https://gitlab.com/tibber1/tibber-client) and run `npm run dev`.
The application should start running on your browser in `localhost:3000`.

# Build the application for production

In the server repository:

Use `npm run start` to build the server application for production.

In the client repository:

Use `npm run build` to build the client application for production.
