import dotenv from 'dotenv';
import express from 'express';

import * as routes from './routes';
import * as authMiddle from './middleware/auth';

dotenv.config();

const port = process.env.SERVER_PORT;

const app = express();

authMiddle.register(app);

routes.register(app);


app.listen(port, () => {
  console.log(`server started at http://localhost:${port}`);
});
