import { Express } from "express-serve-static-core";
import fetch from "node-fetch";

export const register = (app: Express): void => {

  // Saving credentials on env variables is only for this test purpose
  const loginCredentials = {
    email: process.env.USER_EMAIL,
    password: process.env.USER_PASSWORD,
  };

  const authenticate = async (): Promise<{ token: string }> => {
    try {
      const res = await fetch(`${process.env.API_URL}/login.credentials`, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(loginCredentials),
      });
      return res.json();
    } catch (e) {
      console.error(e);
      return e;
    }
  };

  app.use(async (_req, _res, next) => {
    if (app.locals.token) {
      return next();
    }

    const res = await authenticate();
    app.locals.token = res.token;
    next();
  });
};
