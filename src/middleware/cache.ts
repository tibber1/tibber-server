import Express from "express-serve-static-core";
import { NextFunction } from "express";
import NodeCache from "node-cache";

export const nodeCache = (cache: NodeCache) => (
  req: Express.Request,
  res: Express.Response,
  next: NextFunction
): void => {
  const cacheKey = "__Home__" + req.url;
  const cachedValue = cache.get(cacheKey);

  if (cachedValue) {
    res.send(cachedValue);
    return;
  }
  next();
};
