import * as express from "express";
import fetch from "node-fetch";
import NodeCache from "node-cache";
import { nodeCache } from "../middleware/cache";

const cacheTtl = Number.parseInt(process.env.CACHE_DURATION as string);
const cache = new NodeCache({ stdTTL: cacheTtl, checkperiod: 0 });

export const register = (app: express.Application): void => {
  app.get("/home/", nodeCache(cache), async (req, res) => {
    const cacheKey = "__Home__" + req.url;
    const query = {
      query: `
        {
          me {
          home(id: "a8c210fc-2988-4f06-9fe9-ab1bad9529d5") {
            weather {
              minTemperature,
              maxTemperature,
              entries {
                time,
                temperature,
                type
              }
            }
          } 
        }
      }`,
    };

    try {
      const homeByIdResponse = await fetch(`${process.env.API_URL}/gql?`, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          Authorization: app.locals.token,
        },
        body: JSON.stringify(query),
      });

      const data = await homeByIdResponse.json();
      cache.set(cacheKey, data);
      res.send(data);
    } catch (e) {
      console.error(e);
      return e;
    }
  });
};
